Drupal.behaviors.myBehavior = {
  attach: function (context, settings) {

    jQuery(once('hyvorLastComments','#hyvor-talk-last-comments', context)).each(function () {

      // TODO ugly code!
      jQuery.date = function(dateObject) {
        var d = new Date(dateObject * 1000);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        var hour = d.getHours();
        var min = d.getMinutes();

        if (day < 10) {
          day = "0" + day;
        }
        if (month < 10) {
          month = "0" + month;
        }
        if (hour < 10) {
          hour = "0" + hour;
        }
        if (min < 10) {
          min = "0" + min;
        }

        var date = year + "/" + month + "/" + day + ' ' + hour + ':' + min;
        return date;
      };

      var converter = new showdown.Converter();
      jQuery.getJSON( "https://talk.hyvor.com/api/v1/comments?website_id=" + jQuery(this).data("hyvor-talk-website-id")  + "&limit=" + jQuery(this).data("hyvor-talk-last-comments-limit")  + "&type=comments_replies", function( respond ) {
        var items = [];
        jQuery.each( respond.data, function( key, val ) {
          items.push( '<li class="hyvortalk-widget-item">' +
                      '<img class="hyvortalk-widget-avatar" src="' + val.user.picture + '">' +
                      '<strong>' + val.user.name  + '</strong> ' +
                      '<span class="hyvortalk-widget-comment">' +  converter.makeHtml(val.markdown) + '</span>' +
                      '<p class="hyvortalk-widget-meta"><a href="' + val.page.url  +  '?ht-comment-id=' +  val.id +  '">' + val.page.title  + '</a>&nbsp;·&nbsp;' + jQuery.date( val.created_at )  + '</p>' +
                      '</li>' );
        });
        jQuery( "<ul/>", {
          "class": "hyvortalk-widget-list",
          html: items.join("")
        }).appendTo( '#hyvor-talk-last-comments' );
      });
    });

  }
};


Drupal.behaviors.myBehavior2 = {
  attach: function (context, settings) {

    jQuery(once('hyvorTopCommenters','#hyvor-talk-top-commenters', context)).each(function () {

      jQuery.getJSON( "https://talk.hyvor.com/api/data/v1/users?website_id=" + jQuery(this).data("hyvor-talk-website-id")  + "&limit=" + jQuery(this).data("hyvor-talk-top-commenters-limit")  + "&sort=comments_count", function( respond ) {
        var items = [];
        jQuery.each( respond.original, function( key, val ) {
          items.push( '<li class="hyvortalk-widget-item">' +
              '<img class="hyvortalk-widget-avatar" src="' + val.picture_url + '">' +
              '<strong>' + val.name  + '</strong> ' +
              '<span class="hyvortalk-widget-comment-count">(' +  val.comments_count + ')</span>' +
              '</li>' );
        });
        jQuery( "<ul/>", {
          "class": "hyvortalk-widget-list",
          html: items.join("")
        }).appendTo( '#hyvor-talk-top-commenters' );
      });
    });

  }
};




