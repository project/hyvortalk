# HyvorTalk

Hyvor Talk is an ad-free, fully-customizable, beautiful commenting platform.

https://talk.hyvor.com/

**This is not an official module from the guys at Hyvor. It was created by me, a happy Hyvor Talk customer. :)**

This is a very simple module, which makes Hyvor Talk integration easy into Drupal 8/9 websites. It creates a block with the embed code. It uses Drupal content ID as Hyvor Talk page ID, and uses Drupal site's language code. In a multi-language site, the same comments are loaded for all language versions, but with different Hyvor Talk UI languages. On the module config page users can set their Hyvor Talk website ID and can choose from comment loading modes.

### Single Sign-On (SSO)

SSO can be used to allow users to use the comments section without having a Hyvor account but an account on your Drupal website. This module implements Hyvor Talk Stateless SSO method.

### Additional blocks

Currently there is only "Last comments" block.
Comments are loaded by Javascript ajax call from the Hyvor Talk API. You don't need an API key, but at the API settings on the Hyvor console you have to enable "Public Access".

### Installation:

1) Download and enable the module as usual
2) Register on [talk.hyvor.com](https://talk.hyvor.com/) and add your website at hyvor talk console. You will get a website ID.
3) Go to the module configuration page (/admin/config/hyvortalk/config), where you have to add your Hyvor Talk website ID and choose from comment loading modes.
4) If you want to use SSO, add your private key here, which can be generated at Hyvor Talk console.
5) Place Hyvor Talk block to any of your node contents. Block automatically sends Drupal's node ID as page ID to Hyvor and automatically sets Hyvor Talk UI language. If Hyvor doesn't have translations for your Drupal's site language, it loads the English version UI.
5) Visit drupal permissions, and set which roles you would like to have the ability to view Hyvor comments
6) You can customize the appearance and change settings of Hyvor Talk at the Hyvor Talk console

### Loading modes

- **Default**: Comments are loaded on the page load.
- **Scroll**: Comments are loaded when the user scrolls down to the comment section.
- **Click**: Comments are loaded when the user clicks a button. This button's ID is #hyvor-talk-load-button, so you can style it with CSS.

The downside of **scroll** and **click** load mode is that they prevent search engines from indexing the comments. Comments can contribute to your content and help you rank for more keywords. If SEO matters on your website, we recommend using default loading.

### Planned features

- A different approach to integrate hyvor talk: Hyvor Talk comments field 
- Top commenters block