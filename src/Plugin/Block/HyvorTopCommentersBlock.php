<?php

namespace Drupal\hyvortalk\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Block(
 *   id = "hyvor_talk_top_commenters_block",
 *   admin_label = @Translation("Hyvor Talk top commenters"),
 * )
 */

class HyvorTopCommentersBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

        $config = \Drupal::config('hyvortalk.settings');
        $website_id = $config->get('hyvortalk.hyvor_talk_website_id');
        $block_config = $this->getConfiguration();
        return [
            '#theme' => 'hyvor_top_commenters_template',
            '#lang' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
            '#hyvor_talk_website_id' => $website_id,
            '#hyvor_talk_top_commenters_limit' => !empty($block_config["hyvor_talk_top_commenters_limit"]) ? $block_config["hyvor_talk_top_commenters_limit"] : 10,
            '#attached' => [
                'library' => [
                    'hyvortalk/hyvortalk',
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        $form['hyvor_talk_top_commenters_limit'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Users in the toplist'),
            '#description' => $this->t('Number of users to show in this block'),
            '#default_value' => $config['hyvor_talk_top_commenters_limit'] ?? '10',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);
        $values = $form_state->getValues();
        $this->configuration['hyvor_talk_top_commenters_limit'] = $values['hyvor_talk_top_commenters_limit'];
    }

    /**
     * {@inheritdoc}
     */
    public function blockValidate($form, FormStateInterface $form_state) {
        if(!ctype_digit($form_state->getValue('hyvor_talk_top_commenters_limit'))){
            $form_state->setErrorByName('hyvor_talk_top_commenters_limit', $this->t('Limit must be a number!'));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function blockAccess(AccountInterface $account) {
        return AccessResult::allowedIfHasPermission($account, 'access hyvor talk comments');
    }

}

