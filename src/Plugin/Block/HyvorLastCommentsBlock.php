<?php

namespace Drupal\hyvortalk\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Block(
 *   id = "hyvor_talk_last_comments_block",
 *   admin_label = @Translation("Hyvor Talk last comments"),
 * )
 */

class HyvorLastCommentsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = \Drupal::config('hyvortalk.settings');
    $website_id = $config->get('hyvortalk.hyvor_talk_website_id');
    $block_config = $this->getConfiguration();
    return [
        '#theme' => 'hyvor_last_comments_template',
        '#lang' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
        '#hyvor_talk_website_id' => $website_id,
        '#hyvor_talk_last_comments_limit' => !empty($block_config["hyvor_talk_last_comments_limit"]) ? $block_config["hyvor_talk_last_comments_limit"] : 25,
        '#attached' => [
          'library' => [
            'hyvortalk/showdown',
            'hyvortalk/hyvortalk',
          ],
        ],
      ];
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['hyvor_talk_last_comments_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment limit'),
      '#description' => $this->t('Number of last comments to show in this block'),
      '#default_value' => $config['hyvor_talk_last_comments_limit'] ?? '25',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['hyvor_talk_last_comments_limit'] = $values['hyvor_talk_last_comments_limit'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if(!ctype_digit($form_state->getValue('hyvor_talk_last_comments_limit'))){
      $form_state->setErrorByName('hyvor_talk_last_comments_limit', $this->t('Limit must be a number!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access hyvor talk comments');
  }

}

