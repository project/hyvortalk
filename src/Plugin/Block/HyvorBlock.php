<?php

namespace Drupal\hyvortalk\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * @Block(
 *   id = "hyvor_talk_block",
 *   admin_label = @Translation("Hyvor Talk comments"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node")
 *   }
 * )
 */

class HyvorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = \Drupal::config('hyvortalk.settings');

    if ( $config->get('hyvortalk.hyvor_talk_sso')  ){

      $userData = [];
      if ( \Drupal::currentUser()->isAuthenticated() ) {

        $userid = \Drupal::currentUser()->id();
        $user = \Drupal\user\Entity\User::load($userid);

        if (!$user->user_picture->isEmpty()) {
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('thumbnail');
          $user_picture_thumb_url = $style->buildUrl($user->user_picture->entity->getFileUri());
        }
        else{
           $user_picture_thumb_url = '';
        }

        $userData = [
          'id' => $userid,
          'name' => $user->getDisplayName(),
          'email' => $user->getEmail(),
          'picture' => $user_picture_thumb_url,
          'url' => \Drupal::request()->getSchemeAndHttpHost() . '/user/' . $userid,
        ];
      }
      $encodedUserData = base64_encode(json_encode($userData));
      $hash = hash_hmac('sha1', $encodedUserData, $config->get('hyvortalk.hyvor_talk_sso_key'));
    }

    $node = $this->getContextValue('node');
    $website_id = $config->get('hyvortalk.hyvor_talk_website_id');

    if ( empty($config->get('hyvortalk.hyvor_talk_version')) OR $config->get('hyvortalk.hyvor_talk_version') == 2 ) $hyvor_talk_version = 2;
    else $hyvor_talk_version = 3;

    $hyvor_template = $hyvor_talk_version == 2 ? 'hyvor_template' : 'hyvor_template_v3';

    if ( $hyvor_talk_version == 2  )
        $hyvor_talk_load_mode = $config->get('hyvortalk.hyvor_talk_load_mode');
    else {
        if ( $config->get('hyvortalk.hyvor_talk_load_mode') == 'scroll' ) $hyvor_talk_load_mode = 'lazy';
        else $hyvor_talk_load_mode = 'default';
    }

    if ( empty($website_id) )
      return [
        '#markup' => 'Hyvor Talk website ID is not defined in module configuration page!'
      ];
    else
      return [
        '#theme' => $hyvor_template,
        '#nid' => $node->id(),
        '#lang' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
        '#hyvor_talk_website_id' => $website_id,
        '#hyvor_talk_load_mode' => $hyvor_talk_load_mode,
        '#hyvor_talk_sso_enabled' => $config->get('hyvortalk.hyvor_talk_sso') ? 1 : 0,
        '#hyvor_talk_sso_user_data' => empty($encodedUserData) ? '' : $encodedUserData,
        '#hyvor_talk_sso_hash' => empty($hash) ? '' : $hash,
      ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access hyvor talk comments');
  }

}
