<?php

/**
 * @file
 * Contains \Drupal\hyvortalk\Form\HyvorTalkConfigForm.
 */

namespace Drupal\hyvortalk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class HyvorTalkConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hyvortalk_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('hyvortalk.settings');
    $form['hyvor_talk_website_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Hyvor Talk website ID'),
      '#default_value' => $config->get('hyvortalk.hyvor_talk_website_id'),
      '#required' => TRUE,
      '#description' => $this->t('Register on <a href="https://talk.hyvor.com?aff=7905" target="_blank">talk.hyvor.com</a> and add your website at Hyvor Talk console. You will get the website ID.')
    );

      $form['hyvor_talk_version'] = array(
          '#type' => 'radios',
          '#title' => $this->t('Hyvor version to use'),
          '#options' => array(2 => 'v2', 3 => 'v3'),
          '#default_value' => $config->get('hyvortalk.hyvor_talk_version'),
          '#description' => $this->t('Hyvor version to use'),
      );


    $form['hyvor_talk_load_mode'] = array(
      '#type' => 'select',
      '#options' => ['default' => 'Default', 'scroll' => 'Scroll', 'click' => 'Click (v2 only)'],
      '#title' => $this->t('Comment loading mode'),
      '#default_value' => $config->get('hyvortalk.hyvor_talk_load_mode'),
      '#required' => TRUE,
      '#description' => $this->t('<strong>Default:</strong> Comments are loaded on the page load.<br><strong>Scroll:</strong> Comments are loaded when the user scrolls down to the comment section.<br><strong>Click:</strong> Comments are loaded when the user clicks a button.<br>The downside of scroll and click load mode is that they prevent search engines from indexing the comments.  Comments can contribute to your content and help you rank for more keywords. If SEO matters on your website, we recommend using default loading.')
    );

    $form['sso'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Single Sing-On settings'),
    );

    $form['sso']['hyvor_talk_sso'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Single Sing-On (SSO)'),
      '#default_value' => $config->get('hyvortalk.hyvor_talk_sso'),
      '#description' => $this->t('Stateless SSO can be used to allow users to use the comments section without having a Hyvor account but an account on your Drupal website. You have to enable SSO on Hyvor Talk Console too. After enabling SSO you will get a private key. SSO is only available in the business plan!'),
    );

    $form['sso']['hyvor_talk_sso_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SSO private key'),
      '#description' => $this->t('If you enabled SSO, enter your SSO private key here'),
      '#default_value' => $config->get('hyvortalk.hyvor_talk_sso_key'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('hyvortalk.settings');
    $config->set('hyvortalk.hyvor_talk_website_id', $form_state->getValue('hyvor_talk_website_id'));
    $config->set('hyvortalk.hyvor_talk_version', $form_state->getValue('hyvor_talk_version'));
    $config->set('hyvortalk.hyvor_talk_load_mode', $form_state->getValue('hyvor_talk_load_mode'));
    $config->set('hyvortalk.hyvor_talk_sso', $form_state->getValue('hyvor_talk_sso'));
    $config->set('hyvortalk.hyvor_talk_sso_key', $form_state->getValue('hyvor_talk_sso_key'));
    $config->save();
    \Drupal::service('cache.render')->invalidateAll();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('hyvor_talk_website_id'))){
      $form_state->setErrorByName('hyvor_talk_website_id', $this->t('The Hyvor Talk website ID should be a positive integer!'));
    }
    if (!empty($form_state->getValue('hyvor_talk_sso')) AND empty($form_state->getValue('hyvor_talk_sso_key')) ){
        $form_state->setErrorByName('hyvor_talk_sso_key', $this->t('If you want to enable SSO, you have to add your SSO private key!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hyvortalk.settings',
    ];
  }

}
